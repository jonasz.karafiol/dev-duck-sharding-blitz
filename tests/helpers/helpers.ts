import { faker } from '@faker-js/faker';

export const getRandomUserData = () => {
  return {
    username: faker.internet.userName(),
    email: faker.internet.exampleEmail(),
    password: faker.internet.password(),
  };
};

export const getRandomArticleData = () => {
  return {
    title: faker.lorem.words(),
    description: faker.lorem.sentence(),
    body: faker.lorem.paragraphs(),
  };
};
