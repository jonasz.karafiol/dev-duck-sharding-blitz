import { test, expect } from '@playwright/test';
import { getRandomArticleData } from './helpers/helpers';

test.use({ storageState: 'playwright/.auth/user.json' });

test.describe('Article tests', () => {
  for (let i = 0; i < 180; ++i) {
    test(`should be able to add article - ${i}`, async ({ page }) => {
      const article = getRandomArticleData();
      await page.goto('/article/editor');
      await page.getByTestId('article-title').fill(article.title);
      await page.getByTestId('article-description').fill(article.description);
      await page.getByTestId('article-body').fill(article.body);
      await page.getByRole('button', { name: 'Publish Article' }).click();
      await expect(page.locator('#introducing-ionic')).toHaveText(
        article.title
      );
    });

    test(`should display larger numbers for article favourites - ${i}`, async ({
      page,
    }) => {
      await page.goto('');
      page.route('**/favorite**', async route => {
        const response = await route.fetch();
        await route.fulfill({ response, body: '10000' });
      });
      await page.getByTestId('favorite-counter').first().click();
      await expect(page.getByTestId('favorite-counter').first()).toHaveText(
        '10000'
      );
    });
  }
});
