import { test as setup } from '@playwright/test';
import { getRandomUserData } from './helpers/helpers';

const authFile = 'playwright/.auth/user.json';

setup('authenticate', async ({ request }) => {
  const user = getRandomUserData();

  await request.post('/register', {
    form: {
      username: user.username,
      email: user.email,
      password: user.password,
    },
  });

  await request.post('/login', {
    form: {
      username: user.username,
      password: user.password,
    },
  });

  await request.storageState({ path: authFile });
});
