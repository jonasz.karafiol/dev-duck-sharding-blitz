import { test, expect, chromium } from '@playwright/test';
import { getRandomArticleData, getRandomUserData } from './helpers/helpers';

test.describe('Sign up tests', () => {
  for (let i = 0; i < 180; ++i) {
    test(`should sign up and allow for signing in -${i}`, async ({ page }) => {
      const user = getRandomUserData();
      await page.goto('/register');
      await page.getByTestId('username').fill(user.username);
      await page.getByTestId('email').fill(user.email);
      await page.getByTestId('password').fill(user.password);
      await page.getByRole('button', { name: 'Sign up' }).click();
      await expect(
        page.getByRole('heading', { name: 'Sign in' })
      ).toBeVisible();

      await page.getByTestId('username').fill(user.username);
      await page.getByTestId('password').fill(user.password);
      await page.getByRole('button', { name: 'Sign in' }).click();

      await page.getByTestId('user-settings').click();
      await expect(page.getByPlaceholder('Your Name')).toHaveValue(
        user.username
      );
    });
  }
});
