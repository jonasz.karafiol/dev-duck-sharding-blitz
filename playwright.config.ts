import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  testDir: 'tests',
  reportSlowTests: null,
  forbidOnly: !!process.env.CI,
  workers: process.env.CI ? 2 : undefined, // remember to set it according to your environment
  fullyParallel: true,
  reporter: [[process.env.CI ? 'blob' : 'dot'], ['list']],
  maxFailures: 5,
  use: {
    ignoreHTTPSErrors: true,
    baseURL: 'http://localhost:3000',
    trace: 'retain-on-failure',
  },
  projects: [
    {
      name: 'setup',
      testMatch: /.*setup.ts/,
    },
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
      dependencies: ['setup'],
    },
    {
      name: 'iphone 14 pro max',
      use: {
        ...devices['iPhone 14 Pro Max'],
      },
      dependencies: ['setup'],
    },
  ],
  webServer: {
    command: 'bun start',
    url: 'http://localhost:3000',
    reuseExistingServer: !process.env.CI,
    ignoreHTTPSErrors: true,
  },
});
