FROM mcr.microsoft.com/playwright:v1.41.1-jammy

RUN npm install -g bun

RUN bun x playwright install --with-deps
RUN bun i @playwright/test@1.41.2